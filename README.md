Dockerized tomcat/jersey webapp. To run, make sure you are at root directory of the project and run:

sudo docker run -it -p 8080:8080 -v $(pwd):/home/docker/code zezuladp/corenlp-rest

This will compile and run the web application defined in the repository on localhost port 8080 (can change the first number after -p if this port is used). It uses the build.gradle file to fetch and compile dependencies.

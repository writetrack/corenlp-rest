package jayray.net.hello;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import java.io.*;
import java.util.*;

import edu.stanford.nlp.io.*;
import edu.stanford.nlp.ling.*;
import edu.stanford.nlp.pipeline.*;
import edu.stanford.nlp.trees.*;
import edu.stanford.nlp.util.*;

@Path("echo")
public class EchoResource {

	@GET
	@Produces("text/plain")
	public String echo(@QueryParam("m") String message) {
		return "echo: " + message;
	}

    @POST
    @Path("/parse")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces("text/plain")
    public Response createDataInJSON(String data) {
        String result = "Empty text";

        StanfordCoreNLP pipeline = new StanfordCoreNLP();
        Annotation annotation = new Annotation(data);

        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        if (sentences != null && sentences.size() > 0) {
            CoreMap sentence = sentences.get(0);
            Tree tree = sentence.get(TreeCoreAnnotations.TreeAnnotation.class);
            result = tree.toString();
        }
        return Response.status(201).entity(result).build();
    }
}

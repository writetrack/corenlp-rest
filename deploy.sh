#!/bin/bash
set -x \
    && curl -fSL "$TOMCAT_TGZ_URL" -o tomcat.tar.gz \
    && curl -fSL "$TOMCAT_TGZ_URL.asc" -o tomcat.tar.gz.asc \
    && gpg --verify tomcat.tar.gz.asc \
    && tar -xvf tomcat.tar.gz --strip-components=1 \
    && rm bin/*.bat \
    && rm tomcat.tar.gz*

cd /home/docker/code
gradle build
cp /home/docker/code/build/libs/code.war $CATALINA_HOME/webapps/
sh $CATALINA_HOME/bin/catalina.sh run
